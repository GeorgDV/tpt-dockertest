## commands

## Edited for pull request by Georg Daniel Vahtramäe

```sh
# build docker image (named: phptest)
docker build -t phptest .

# run image, expose port 80
docker run --rm -p 80:80 phptest
# -p external:internal   (80:80  / 8011:80)


# list of running containers
docker ps
# kill running container
docker kill <id>
# kill all running docker images
docker kill $(docker ps -q)

#
# use gitlab docker registry
#


# build docker image for project
docker build -t registry.gitlab.com/eritikass/tpt-dockertest .
# push docker image to gitlab docker registry
docker push registry.gitlab.com/eritikass/tpt-dockertest

# pull image
docker pull registry.gitlab.com/eritikass/tpt-dockertest

# run image
docker run --rm -p 80:80 registry.gitlab.com/eritikass/tpt-dockertest
```
