Install:
  - `kubectl` cli -> https://kubernetes.io/docs/tasks/tools/install-kubectl/

```sh
# apply config to kubernetes
kubectl apply -f deployment.yml

# get list of deployments
kubectl get deployments

# get list of pods
kubectl get pods

# show pod log (one pod)
kubectl logs -f <POD_ID>
# show logs, all pods that match selector
kubectl logs -f -l=app=test


# get pods by selector ( app=test )
kubectl get pods -l=app=test

# apply ingress entry + service
# don't forgot to change hostname in ingress
kubectl apply -f service.yml
kubectl apply -f ingress.yml
```

## In every lecture

* In start of each lecture configure kubectl
  - download cluster config from digitalocean
  - place content of that to `~/.kube/config`

```sh
# in the end of lectur delete access token
rm ~/.kube/config

#
# first time
# - Download config from DigitalOcean UI
# 

# create kube config folder
mkdir -p ~/.kube

# move config 
mv ~/Downloads/tpt1-kubeconfig.yaml ~/.kube/config

# test if all ok, should say server version
kubectl version
```

## Install nginx ingress

> https://kubernetes.github.io/ingress-nginx/deploy/

```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.30.0/deploy/static/mandatory.yaml

kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.30.0/deploy/static/provider/baremetal/service-nodeport.yaml

# get IP
kubectl -n ingress-nginx get services

# show logs from nginx ingress
kubectl -n ingress-nginx logs -f --tail=100 deployment/nginx-ingress-controller
```